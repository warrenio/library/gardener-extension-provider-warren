#!/usr/bin/env bash

# Copyright (c) 2020 SAP SE or an SAP affiliate company. All rights reserved.
# Copyright 2022 OYE Network OÜ. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

EXTENSION_PREFIX            := gardener-extension
NAME                        := provider-warren
ADMISSION_NAME              := admission-warren
REPO_ROOT                   := $(shell dirname $(realpath $(lastword ${MAKEFILE_LIST})))
HACK_DIR                    := ${REPO_ROOT}/hack
GARDENER_HACK_DIR           := ${REPO_ROOT}/vendor/github.com/gardener/gardener/hack
KUBECONFIG                  := dev/kubeconfig.yaml
MANAGER_CONFIG_FILE         := example/00-componentconfig.yaml
PROJECT_NAME                := warren.io
LD_FLAGS_CMD                := "${GARDENER_HACK_DIR}/get-build-ld-flags.sh k8s.io/component-base $(REPO_ROOT)/VERSION $(EXTENSION_PREFIX)-$(NAME)"
LEADER_ELECTION             := false
IGNORE_OPERATION_ANNOTATION := false
GARDENER_VERSION            := $(grep "gardener/gardener v" go.mod | tr "[:blank:]" "\\n" | tail -1)

WEBHOOK_CONFIG_PORT	:= 8444
WEBHOOK_CONFIG_MODE	:= url
WEBHOOK_CONFIG_URL	:= kube-host:${WEBHOOK_CONFIG_PORT}
WEBHOOK_CERT_DIR    := ./example/${ADMISSION_NAME}-certs
EXTENSION_NAMESPACE	:=

WEBHOOK_PARAM := --webhook-config-url=${WEBHOOK_CONFIG_URL}
ifeq (${WEBHOOK_CONFIG_MODE}, service)
  WEBHOOK_PARAM := --webhook-config-namespace=${EXTENSION_NAMESPACE}
endif

#########################################
# Rules for local development scenarios #
#########################################

.PHONY: start
start: setup-gardener-scripts
	@LEADER_ELECTION_NAMESPACE=garden GO111MODULE=on go run \
		-mod=vendor \
		-ldflags "-w $(shell $(shell echo ${LD_FLAGS_CMD}))" \
		./cmd/${EXTENSION_PREFIX}-${NAME} \
		--kubeconfig=${KUBECONFIG} \
		--config-file=${MANAGER_CONFIG_FILE} \
		--ignore-operation-annotation=${IGNORE_OPERATION_ANNOTATION} \
		--leader-election=${LEADER_ELECTION} \
		--webhook-config-server-host=0.0.0.0 \
		--webhook-config-server-port=${WEBHOOK_CONFIG_PORT} \
		--webhook-config-mode=${WEBHOOK_CONFIG_MODE} \
		--gardener-version=${GARDENER_VERSION} \
		${WEBHOOK_PARAM}

.PHONY: debug
debug:
	dlv debug  ./cmd/${EXTENSION_PREFIX}-${NAME} -- \
		--kubeconfig=${KUBECONFIG} \
		--config-file=${MANAGER_CONFIG_FILE} \
		--ignore-operation-annotation=${IGNORE_OPERATION_ANNOTATION} \
		--leader-election=${LEADER_ELECTION} \
		--webhook-config-server-host=0.0.0.0 \
		--webhook-config-server-port=${WEBHOOK_CONFIG_PORT} \
		--webhook-config-mode=${WEBHOOK_CONFIG_MODE} \
		--gardener-version=${GARDENER_VERSION} \
		${WEBHOOK_PARAM}

.PHONY: start-admission
start-admission: setup-gardener-scripts
	@LEADER_ELECTION_NAMESPACE=garden GO111MODULE=on go run \
		-mod=vendor \
		-ldflags "-w $(shell $(shell echo ${LD_FLAGS_CMD}))" \
		./cmd/${EXTENSION_PREFIX}-${ADMISSION_NAME} \
		--kubeconfig=${KUBECONFIG} \
		--leader-election=${LEADER_ELECTION} \
		--webhook-config-server-host=0.0.0.0 \
		--webhook-config-server-port=$(WEBHOOK_CONFIG_PORT) \
		--webhook-config-mode=${WEBHOOK_CONFIG_MODE} \
		${WEBHOOK_PARAM}

.PHONY: debug-admission
debug-admission:
	LEADER_ELECTION_NAMESPACE=garden dlv debug \
		./cmd/${EXTENSION_PREFIX}-${ADMISSION_NAME} -- \
		--leader-election=${LEADER_ELECTION} \
		--kubeconfig=${KUBECONFIG} \
		--webhook-config-server-host=0.0.0.0 \
		--webhook-config-server-port=$(WEBHOOK_CONFIG_PORT) \
		--webhook-config-mode=${WEBHOOK_CONFIG_MODE} \
		${WEBHOOK_PARAM}

.PHONY: setup-gardener-scripts
setup-gardener-scripts:
	@chmod +x ${GARDENER_HACK_DIR}/*
	@chmod +x ${REPO_ROOT}/vendor/k8s.io/code-generator/kube_codegen.sh

.PHONY: update-codegen
update-codegen: setup-gardener-scripts
	@${HACK_DIR}/update-codegen.sh

#########################################
# Rules for re-vendoring
#########################################

.PHONY: revendor
revendor:
	@GO111MODULE=on go mod tidy -compat=1.17
	@GO111MODULE=on go mod vendor

.PHONY: update-dependencies
update-dependencies:
	@env GO111MODULE=on go get -u

#########################################
# Rules for testing
#########################################

.PHONY: test
test:
	${HACK_DIR}/test.sh

.PHONY: test-cov
test-cov:
	${HACK_DIR}/test.sh --coverage

.PHONY: test-clean
test-clean:
	${HACK_DIR}/test.sh --clean --coverage

#########################################
# Rules for build/release
#########################################

.PHONY: build-local
build-local: setup-gardener-scripts
	@env LD_FLAGS="-w $(shell $(shell echo ${LD_FLAGS_CMD}))" LOCAL_BUILD=1 ${HACK_DIR}/build.sh

.PHONY: build
build: setup-gardener-scripts
	@env LD_FLAGS="-w $(shell $(shell echo ${LD_FLAGS_CMD}))" ${HACK_DIR}/build.sh

.PHONY: generate-controller-registration
generate-controller-registration: setup-gardener-scripts
	@${GARDENER_HACK_DIR}/generate-controller-registration.sh provider-warren charts/gardener-extension-provider-warren ${VERSION} controller-registration.yaml ControlPlane:warren Infrastructure:warren Worker:warren

.PHONY: clean
clean: setup-gardener-scripts
	@${GARDENER_HACK_DIR}/clean.sh ./cmd/... ./pkg/... ./test/... ./tmp/

#########################################
# Rules for verification
#########################################

.PHONY: check-generate
check-generate: setup-gardener-scripts
	@${GARDENER_HACK_DIR}/check-generate.sh ${REPO_ROOT}

.PHONY: check
check: setup-gardener-scripts
	@${GARDENER_HACK_DIR}/check.sh --golangci-lint-config=./.golangci.yaml ./cmd/... ./pkg/... ./test/...
	@${GARDENER_HACK_DIR}/check-charts.sh ./charts

.PHONY: generate
generate: setup-gardener-scripts
	@env HACK_DIR=${HACK_DIR} REPO_ROOT=${REPO_ROOT} ${GARDENER_HACK_DIR}/generate-sequential.sh ./charts/... ./cmd/... ./pkg/... ./test/...

.PHONY: format
format: setup-gardener-scripts
	@${GARDENER_HACK_DIR}/format.sh ./cmd ./pkg ./test

.PHONY: verify
verify: check format test

.PHONY: verify-extended
verify-extended: check-generate check format test-clean

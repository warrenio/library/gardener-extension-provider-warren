#!/usr/bin/env bash

# Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
# Copyright 2022 OYE Network OÜ. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

HACK_DIR="${HACK_DIR:-$(dirname $0)}"
. $HACK_DIR/prepare-source-path.sh

REPO_ROOT="${REPO_ROOT:-$(dirname $0)/..}"

bash "${REPO_ROOT}/vendor/k8s.io/code-generator/generate-internal-groups.sh" \
  deepcopy,defaulter,conversion \
  gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/client \
  gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren \
  gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren \
  "apis:v1alpha1" \
  --go-header-file "${REPO_ROOT}/docs/LICENSE_BOILERPLATE.txt"

bash "${REPO_ROOT}/vendor/k8s.io/code-generator/generate-internal-groups.sh" \
  deepcopy,defaulter,conversion \
  gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/client/config \
  gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis \
  gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis \
  "config:v1alpha1" \
  --go-header-file "${REPO_ROOT}/docs/LICENSE_BOILERPLATE.txt"

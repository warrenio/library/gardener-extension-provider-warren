/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package mock provides all methods required to simulate a Warren Platform provider environment
package mock

import (
	"fmt"
	"net/http"
	"net/http/httptest"

	mockkubernetes "github.com/gardener/gardener/pkg/client/kubernetes/mock"
	mockclient "github.com/gardener/gardener/pkg/mock/controller-runtime/client"
	mockmanager "github.com/gardener/gardener/pkg/mock/controller-runtime/manager"
	"github.com/onsi/ginkgo/v2"
	"gitlab.com/warrenio/library/go-client/warren"
	"go.uber.org/mock/gomock"
)

const (
	TestFloatingPoolName = "MY-FLOATING-POOL"
	TestNamespace        = "test-namespace"
	TestNetworkUUID      = "23456789-abcd-4f01-23e5-6789abcdef01"
	TestRegion           = "cyc"
	TestSSHPublicKey     = "ecdsa-sha2-nistp384 AAAAE2VjZHNhLXNoYTItbmlzdHAzODQAAAAIbmlzdHAzODQAAABhBJ9S5cCzfygWEEVR+h3yDE83xKiTlc7S3pC3IadoYu/HAmjGPNRQZWLPCfZe5K3PjOGgXghmBY22voYl7bSVjy+8nZRPuVBuFDZJ9xKLPBImQcovQ1bMn8vXno4fvAF4KQ=="
	TestZone             = "cyc01"
)

// MockTestEnv represents the test environment for testing Warren Platform API calls
type MockTestEnv struct {
	ChartApplier   *mockkubernetes.MockChartApplier
	Client         *mockclient.MockClient
	Manager        *mockmanager.MockManager
	MockController *gomock.Controller
	StatusWriter   *mockclient.MockStatusWriter

	Server       *httptest.Server
	Mux          *http.ServeMux
	WarrenClient *warren.Client
}

// Teardown shuts down the test environment
func (env *MockTestEnv) Teardown() {
	env.MockController.Finish()

	env.ChartApplier = nil
	env.Client = nil
	env.MockController = nil
	env.StatusWriter = nil

	env.Server.Close()

	env.Server = nil
	env.Mux = nil
	env.WarrenClient = nil
}

// NewMockTestEnv generates a new, unconfigured test environment for testing purposes.
func NewMockTestEnv() MockTestEnv {
	ctrl := gomock.NewController(ginkgo.GinkgoT())

	client := mockclient.NewMockClient(ctrl)
	manager := mockmanager.NewMockManager(ctrl)
	manager.EXPECT().GetClient().Return(client).AnyTimes()

	mux := http.NewServeMux()
	server := httptest.NewServer(mux)

	warrenClient, err := (&warren.ClientBuilder{}).ApiUrl(fmt.Sprintf("%s/v1", server.URL)).ApiToken("dummy-token").LocationSlug(TestZone).Build()
	if nil != err {
		panic(err)
	}

	return MockTestEnv{
		ChartApplier:   mockkubernetes.NewMockChartApplier(ctrl),
		Client:         client,
		Manager:        manager,
		MockController: ctrl,
		StatusWriter:   mockclient.NewMockStatusWriter(ctrl),

		Server:       server,
		Mux:          mux,
		WarrenClient: warrenClient,
	}
}

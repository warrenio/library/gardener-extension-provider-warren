/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package mock provides all methods required to simulate a Warren Platform provider environment
package mock

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gardener/gardener/pkg/apis/extensions/v1alpha1"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const (
	jsonNetworkDataTemplate = `
{
    "vlan_id": 42,
    "subnet": "10.42.0.0/24",
    "name": "test-namespace-cyc01-workers",
    "created_at": "2021-06-29 08:22:52",
    "updated_at": "2021-06-29 08:22:52",
    "uuid": %q,
    "type": "private",
    "is_default": true,
    "vm_uuids": [],
    "resources_count": 0
}
	`
	TestInfrastructureName = "abc"
	TestInfrastructureNetworkName = "test-namespace-cyc01-workers"
	TestInfrastructureProviderConfig = `
{
	"apiVersion": "warren.provider.extensions.gardener.cloud/v1alpha1",
	"kind": "InfrastructureConfig",
	"floatingPoolName": "MY-FLOATING-POOL"
}
	`
	TestInfrastructureSecretName = "cloudprovider"
)

// NewInfrastructure generates a new provider specification for testing purposes.
func NewInfrastructure() *v1alpha1.Infrastructure {
	return &v1alpha1.Infrastructure{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "extensions.gardener.cloud",
			Kind:       "Infrastructure",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      TestInfrastructureName,
			Namespace: TestNamespace,
		},
		Spec: v1alpha1.InfrastructureSpec{
			Region: TestRegion,
			SecretRef: corev1.SecretReference{
				Name: TestInfrastructureSecretName,
				Namespace: TestNamespace,
			},
			DefaultSpec: v1alpha1.DefaultSpec{
				ProviderConfig: &runtime.RawExtension{
					Raw: []byte(TestInfrastructureProviderConfig),
				},
			},
			SSHPublicKey: []byte(TestSSHPublicKey),
		},
	}
}

// NewInfrastructureConfigSpec generates a new infrastructure config specification for testing purposes.
func NewInfrastructureConfigSpec() *apis.InfrastructureConfig {
	return &apis.InfrastructureConfig{ FloatingPoolName: TestFloatingPoolName }
}

// ManipulateInfrastructure changes given provider specification.
//
// PARAMETERS
// infrastructure *extensions.Infrastructure Infrastructure specification
// data           map[string]interface{}     Members to change
func ManipulateInfrastructure(infrastructure *v1alpha1.Infrastructure, data map[string]interface{}) *v1alpha1.Infrastructure {
	for key, value := range data {
		if (strings.Index(key, "ObjectMeta") == 0) {
			manipulateStruct(&infrastructure.ObjectMeta, key[11:], value)
		} else if (strings.Index(key, "Spec") == 0) {
			manipulateStruct(&infrastructure.Spec, key[7:], value)
		} else if (strings.Index(key, "TypeMeta") == 0) {
			manipulateStruct(&infrastructure.TypeMeta, key[9:], value)
		} else {
			manipulateStruct(&infrastructure, key, value)
		}
	}

	return infrastructure
}

// SetupNetworkEndpointOnMux configures a "/networks" endpoint on the mux given.
//
// PARAMETERS
// mux *http.ServeMux Mux to add handler to
func SetupNetworkEndpointOnMux(mux *http.ServeMux, emptyUntilCreated bool) {
	baseURL := "/v1/cyc01/network"
	isNetworkCreated := !emptyUntilCreated

	mux.HandleFunc(fmt.Sprintf("%s/network", baseURL), func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Type", "application/json; charset=utf-8")

		if (strings.ToLower(req.Method) == "post") {
			isNetworkCreated = true
			res.WriteHeader(http.StatusCreated)

			res.Write([]byte(fmt.Sprintf(jsonNetworkDataTemplate, TestNetworkUUID)))
		} else {
			panic("Unsupported HTTP method call")
		}
	})

	mux.HandleFunc(fmt.Sprintf("%s/networks", baseURL), func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Type", "application/json; charset=utf-8")

		res.WriteHeader(http.StatusOK)
		res.Write([]byte("["))

		if isNetworkCreated {
			res.Write([]byte(fmt.Sprintf(jsonNetworkDataTemplate, TestNetworkUUID)))
		}

		res.Write([]byte("]"))
	})
}

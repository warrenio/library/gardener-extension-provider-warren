/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis is the main package for Warren specific APIs
package apis

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/warrenio/library/go-client/warren"
)

func GetNetworkByName(ctx context.Context, client *warren.Client, name string) (*warren.Network, error) {
	networks, err := client.Network.ListNetworks()
	if err != nil {
		return nil, GetNetworkErrorFromHttpCallError(err)
	}

	for _, network := range *networks {
		if network.Name == name {
			return &network, nil
		}
	}

	return nil, fmt.Errorf("%w: %s", ErrNetworkNotFound, name)
}

func GetNetworkErrorFromHttpCallError(err error) error {
	if nil != err {
		errString := err.Error()

		if strings.HasPrefix(errString, "[") && strings.Index(errString, "]") == 4 {
			switch errString[1:4] {
			case "400":
				if strings.Index(errString, "Network UUID is invalid.") > -1 {
					return ErrNetworkNotFound
				}

				return GetErrorFromHttpCallError(err)
			case "404":
				return ErrNetworkNotFound
			default:
				return GetErrorFromHttpCallError(err)
			}
		}
	}

	return err
}

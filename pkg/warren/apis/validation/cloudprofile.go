/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package validation contains functions to validate controller specifications
package validation

import (
	gardencorev1beta1 "github.com/gardener/gardener/pkg/apis/core/v1beta1"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// ValidateCloudProfileConfig validates a CloudProfileConfig object.
func ValidateCloudProfileConfig(profileSpec *gardencorev1beta1.CloudProfileSpec, profileConfig *apis.CloudProfileConfig) field.ErrorList {
	allErrs := field.ErrorList{}
/*
	fldPath := field.NewPath("spec")

	if len(profileConfig.Regions) < 1 {
		allErrs = append(allErrs, field.Required(fldPath.Child("regions"), "is a required field"))
	}
*/
	return allErrs
}

func isSet(s *string) bool {
	return s != nil && *s != ""
}

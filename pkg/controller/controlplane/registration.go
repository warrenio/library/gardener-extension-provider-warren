/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package controlplane contains functions used at the controlplane controller
package controlplane

import (
	"context"
	"sync/atomic"

	extensionscontroller "github.com/gardener/gardener/extensions/pkg/controller"
	"github.com/gardener/gardener/extensions/pkg/controller/controlplane"
	"github.com/gardener/gardener/extensions/pkg/controller/controlplane/genericactuator"
	"github.com/gardener/gardener/extensions/pkg/util"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren"
	controllerapis "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

var (
	// DefaultAddOptions are the default AddOptions for AddToManager.
	DefaultAddOptions = AddOptions{}
)

// AddOptions are options to apply when adding the Warren Platform controlplane controller to the manager.
type AddOptions struct {
	// Controller are the controller.Options.
	Controller controller.Options
	// GardenId is the Gardener garden identity
	GardenId string
	// IgnoreOperationAnnotation specifies whether to ignore the operation annotation or not.
	IgnoreOperationAnnotation bool
	// ShootWebhookConfig specifies the desired Shoot MutatingWebhooksConfiguration.
	ShootWebhookConfig *atomic.Value
	// WebhookServerNamespace is the namespace in which the webhook server runs.
	WebhookServerNamespace string
}

// AddToManagerWithOptions adds a controller with the given Options to the given manager.
// The opts.Reconciler is being set with a newly instantiated actuator.
//
// PARAMETERS
// mgr  manager.Manager Control plane controller manager instance
// opts AddOptions      Options to add
func AddToManagerWithOptions(ctx context.Context, mgr manager.Manager, opts AddOptions) error {
	actuator, err := genericactuator.NewActuator(
		mgr,
		warren.Name,
		getSecretConfigs,
		getShootAccessSecrets,
		nil,
		nil,
		configChart,
		controlPlaneChart,
		controlPlaneShootChart,
		nil,
		storageClassChart,
		nil,
		NewValuesProvider(mgr, opts.GardenId),
		extensionscontroller.ChartRendererFactoryFunc(util.NewChartRendererForShoot),
		controllerapis.ImageVector(),
		"",
		opts.ShootWebhookConfig,
		opts.WebhookServerNamespace,
	)

	if err != nil {
		return err
	}

	return controlplane.Add(
		ctx,
		mgr,
		controlplane.AddArgs{
			Actuator:          actuator,
			ControllerOptions: opts.Controller,
			Predicates:        controlplane.DefaultPredicates(ctx, mgr, opts.IgnoreOperationAnnotation),
			Type:              warren.Type,
		},
	)
}

// AddToManager adds a controller with the default Options.
//
// PARAMETERS
// mgr manager.Manager Control plane controller manager instance
func AddToManager(ctx context.Context, mgr manager.Manager) error {
	return AddToManagerWithOptions(ctx, mgr, DefaultAddOptions)
}

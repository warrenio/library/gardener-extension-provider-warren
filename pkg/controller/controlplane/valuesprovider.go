/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package controlplane contains functions used at the controlplane controller
package controlplane

import (
	"context"
	"fmt"
	"hash/fnv"
	"path/filepath"
	"strconv"
	"strings"

	extensionscontroller "github.com/gardener/gardener/extensions/pkg/controller"
	"github.com/gardener/gardener/extensions/pkg/controller/controlplane/genericactuator"
	extensionssecretsmanager "github.com/gardener/gardener/extensions/pkg/util/secret/manager"
	v1beta1constants "github.com/gardener/gardener/pkg/apis/core/v1beta1/constants"
	extensionsv1alpha1 "github.com/gardener/gardener/pkg/apis/extensions/v1alpha1"
	"github.com/gardener/gardener/pkg/utils/chart"
	gardenerutils "github.com/gardener/gardener/pkg/utils/gardener"
	k8sutils "github.com/gardener/gardener/pkg/utils/kubernetes"
	secretutils "github.com/gardener/gardener/pkg/utils/secrets"
	secretsmanager "github.com/gardener/gardener/pkg/utils/secrets/manager"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/charts"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/transcoder"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	policyv1beta1 "k8s.io/api/policy/v1beta1"
	rbacv1 "k8s.io/api/rbac/v1"
	storagev1 "k8s.io/api/storage/v1"
	autoscalingv1beta2 "k8s.io/autoscaler/vertical-pod-autoscaler/pkg/apis/autoscaling.k8s.io/v1beta2"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

const (
	caNameControlPlane = "ca-" + warren.Name + "-controlplane"
)

var (
	configChart = &chart.Chart{
		Name:       warren.CloudProviderConfigName,
		EmbeddedFS: charts.InternalChart,
		Path:       filepath.Join(charts.InternalChartsPath, "cloud-provider-config"),
		Objects: []*chart.Object{
			{Type: &corev1.Secret{}, Name: warren.CloudProviderConfigName},
		},
	}

	controlPlaneChart = &chart.Chart{
		Name:       "seed-controlplane",
		EmbeddedFS: charts.InternalChart,
		Path:       filepath.Join(charts.InternalChartsPath, "seed-controlplane"),
		SubCharts: []*chart.Chart{
			{
				Name:   warren.CloudControllerManagerName,
				Images: []string{warren.CloudControllerImageName},
				Objects: []*chart.Object{
					{Type: &corev1.Service{}, Name: warren.CloudControllerManagerName},
					{Type: &appsv1.Deployment{}, Name: warren.CloudControllerManagerName},
					{Type: &corev1.ConfigMap{}, Name: warren.CloudControllerManagerName + "-observability-config"},
					{Type: &autoscalingv1beta2.VerticalPodAutoscaler{}, Name: warren.CloudControllerManagerName + "-vpa"},
				},
			},
			{
				Name: warren.CSIControllerName,
				Images: []string{
					warren.CSIAttacherImageName,
					warren.CSIProvisionerImageName,
					warren.CSIDriverControllerImageName,
					warren.CSIResizerImageName,
					warren.LivenessProbeImageName,
				},
				Objects: []*chart.Object{
					{Type: &appsv1.Deployment{}, Name: warren.CSIControllerName},
					{Type: &corev1.ConfigMap{}, Name: warren.CSIControllerName + "-observability-config"},
					{Type: &autoscalingv1beta2.VerticalPodAutoscaler{}, Name: warren.CSIControllerName + "-vpa"},
				},
			},
		},
	}

	controlPlaneShootChart = &chart.Chart{
		Name:       "shoot-system-components",
		EmbeddedFS: charts.InternalChart,
		Path:       filepath.Join(charts.InternalChartsPath, "shoot-system-components"),
		SubCharts: []*chart.Chart{
			{
				Name: warren.CloudControllerManagerName,
				Objects: []*chart.Object{
					{Type: &rbacv1.ClusterRole{}, Name: "system:cloud-controller-manager"},
					{Type: &rbacv1.ClusterRoleBinding{}, Name: "system:cloud-controller-manager"},
					{Type: &rbacv1.ClusterRole{}, Name: "system:controller:cloud-node-controller"},
					{Type: &rbacv1.ClusterRoleBinding{}, Name: "system:controller:cloud-node-controller"},
				},
			},
			{
				Name: warren.CSINodeName,
				Images: []string{
					warren.CSINodeDriverRegistrarImageName,
					warren.CSIDriverNodeImageName,
					warren.LivenessProbeImageName,
				},
				Objects: []*chart.Object{
					// csi-driver
					{Type: &appsv1.DaemonSet{}, Name: warren.CSINodeName},
					{Type: &storagev1.CSIDriver{}, Name: "csi.storage.warren.io"},
					{Type: &corev1.ServiceAccount{}, Name: warren.CSIDriverName},
					{Type: &rbacv1.ClusterRole{}, Name: warren.UsernamePrefix + warren.CSIDriverName},
					{Type: &rbacv1.ClusterRoleBinding{}, Name: warren.UsernamePrefix + warren.CSIDriverName},
					{Type: &policyv1beta1.PodSecurityPolicy{}, Name: strings.Replace(warren.UsernamePrefix+warren.CSIDriverName, ":", ".", -1)},
					{Type: extensionscontroller.GetVerticalPodAutoscalerObject(), Name: warren.CSINodeName},
					// csi-provisioner
					{Type: &rbacv1.ClusterRole{}, Name: warren.UsernamePrefix + warren.CSIProvisionerName},
					{Type: &rbacv1.ClusterRoleBinding{}, Name: warren.UsernamePrefix + warren.CSIProvisionerName},
					{Type: &rbacv1.Role{}, Name: warren.UsernamePrefix + warren.CSIProvisionerName},
					{Type: &rbacv1.RoleBinding{}, Name: warren.UsernamePrefix + warren.CSIProvisionerName},
					// csi-attacher
					{Type: &rbacv1.ClusterRole{}, Name: warren.UsernamePrefix + warren.CSIAttacherName},
					{Type: &rbacv1.ClusterRoleBinding{}, Name: warren.UsernamePrefix + warren.CSIAttacherName},
					{Type: &rbacv1.Role{}, Name: warren.UsernamePrefix + warren.CSIAttacherName},
					{Type: &rbacv1.RoleBinding{}, Name: warren.UsernamePrefix + warren.CSIAttacherName},
				},
			},
		},
	}

	storageClassChart = &chart.Chart{
		Name:       "shoot-storageclasses",
		EmbeddedFS: charts.InternalChart,
		Path:       filepath.Join(charts.InternalChartsPath, "shoot-storageclasses"),
	}
)

func getSecretConfigs(namespace string) []extensionssecretsmanager.SecretConfigWithOptions {
	return []extensionssecretsmanager.SecretConfigWithOptions{
		{
			Config: &secretutils.CertificateSecretConfig{
				Name:       caNameControlPlane,
				CommonName: caNameControlPlane,
				CertType:   secretutils.CACert,
			},
			Options: []secretsmanager.GenerateOption{secretsmanager.Persist()},
		},
		{
			Config: &secretutils.CertificateSecretConfig{
				Name:                        warren.CloudControllerManagerServerName,
				CommonName:                  warren.CloudControllerManagerName,
				DNSNames:                    k8sutils.DNSNamesForService(warren.CloudControllerManagerName, namespace),
				CertType:                    secretutils.ServerCert,
				SkipPublishingCACertificate: true,
			},
			Options: []secretsmanager.GenerateOption{secretsmanager.SignedByCA(caNameControlPlane)},
		},
	}
}

func getShootAccessSecrets(namespace string) []*gardenerutils.AccessSecret {
	return []*gardenerutils.AccessSecret{
		gardenerutils.NewShootAccessSecret(warren.CloudControllerManagerName, namespace),
		gardenerutils.NewShootAccessSecret(warren.CSIAttacherName, namespace),
		gardenerutils.NewShootAccessSecret(warren.CSIProvisionerName, namespace),
		gardenerutils.NewShootAccessSecret(warren.CSIControllerName, namespace),
		gardenerutils.NewShootAccessSecret(warren.CSIResizerName, namespace),
	}
}

// NewValuesProvider creates a new ValuesProvider for the generic actuator.
//
// PARAMETERS
// logger   logr.Logger Logger instance
// gardenID string      Garden ID
func NewValuesProvider(mgr manager.Manager, gardenID string) genericactuator.ValuesProvider {
	return &valuesProvider{
		client:   mgr.GetClient(),
		gardenID: gardenID,
	}
}

// valuesProvider is a ValuesProvider that provides warren-specific values for the 2 charts applied by the generic actuator.
type valuesProvider struct {
	genericactuator.NoopValuesProvider

	client   client.Client
	gardenID string
}

// GetConfigChartValues returns the values for the config chart applied by the generic actuator.
//
// PARAMETERS
// ctx     context.Context                  Execution context
// cp      *extensionsv1alpha1.ControlPlane Control plane struct
// cluster *extensionscontroller.Cluster    Cluster struct
func (vp *valuesProvider) GetConfigChartValues(
	ctx context.Context,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
) (map[string]interface{}, error) {
	cpConfig, err := transcoder.DecodeControlPlaneConfigFromControllerCluster(cluster)
	if err != nil {
		return nil, err
	}

	// Get credentials
	credentials, err := warren.GetCredentials(ctx, vp.client, cp.Spec.SecretRef)
	if err != nil {
		return nil, fmt.Errorf("could not get warren credentials from secret '%s/%s': %w", cp.Spec.SecretRef.Namespace, cp.Spec.SecretRef.Name, err)
	}

	// Get config chart values
	return vp.getConfigChartValues(cpConfig, cp, cluster, credentials)
}

// GetControlPlaneChartValues returns the values for the control plane chart applied by the generic actuator.
//
// PARAMETERS
// ctx           context.Context                  Execution context
// cp            *extensionsv1alpha1.ControlPlane Control plane struct
// cluster       *extensionscontroller.Cluster    Cluster struct
// secretsReader secretsmanager.Reader            Secrets manager reader
// checksums     map[string]string                Checksums
// scaledDown    bool                             True if scaled down
func (vp *valuesProvider) GetControlPlaneChartValues(
	ctx context.Context,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	secretsReader secretsmanager.Reader,
	checksums map[string]string,
	scaledDown bool,
) (map[string]interface{}, error) {
	cpConfig, err := transcoder.DecodeControlPlaneConfigFromControllerCluster(cluster)
	if err != nil {
		return nil, err
	}

	// Decode infrastructureProviderStatus
	infraStatus, err := transcoder.DecodeInfrastructureStatusFromControlPlane(cp)
	if nil != err {
		return nil, fmt.Errorf("could not decode infrastructureProviderStatus of controlplane '%s': %w", k8sutils.ObjectName(cp), err)
	}

	// Get credentials
	credentials, err := warren.GetCredentials(ctx, vp.client, cp.Spec.SecretRef)
	if err != nil {
		return nil, fmt.Errorf("could not get warren credentials from secret '%s/%s': %w", cp.Spec.SecretRef.Namespace, cp.Spec.SecretRef.Name, err)
	}

	// Get control plane chart values
	return vp.getControlPlaneChartValues(cpConfig, infraStatus, cp, cluster, secretsReader, credentials, checksums, scaledDown)
}

// GetControlPlaneShootChartValues returns the values for the control plane shoot chart applied by the generic actuator.
//
// PARAMETERS
// ctx     context.Context                  Execution context
// cp      *extensionsv1alpha1.ControlPlane Control plane struct
// cluster *extensionscontroller.Cluster    Cluster struct
// _       secretsmanager.Reader            Secrets manager reader
// _       map[string]string                Checksums
func (vp *valuesProvider) GetControlPlaneShootChartValues(
	ctx context.Context,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	_ secretsmanager.Reader,
	_ map[string]string,
) (map[string]interface{}, error) {
	cpConfig, err := transcoder.DecodeControlPlaneConfigFromControllerCluster(cluster)
	if err != nil {
		return nil, err
	}

	// Get credentials
	credentials, err := warren.GetCredentials(ctx, vp.client, cp.Spec.SecretRef)
	if err != nil {
		return nil, fmt.Errorf("could not get warren credentials from secret '%s/%s': %w", cp.Spec.SecretRef.Namespace, cp.Spec.SecretRef.Name, err)
	}

	// Get control plane shoot chart values
	return vp.getControlPlaneShootChartValues(cpConfig, cp, cluster, credentials)
}

// GetStorageClassesChartValues returns the values for the shoot storageclasses chart applied by the generic actuator.
//
// PARAMETERS
// _       context.Context                  Execution context
// _       *extensionsv1alpha1.ControlPlane Control plane struct
// cluster *extensionscontroller.Cluster    Cluster struct
func (vp *valuesProvider) GetStorageClassesChartValues(
	_ context.Context,
	_ *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
) (map[string]interface{}, error) {
	cloudProfileConfig, err := transcoder.DecodeCloudProfileConfigFromControllerCluster(cluster)
	if err != nil {
		return nil, err
	}

	volumeBindingMode := "Immediate"

	return map[string]interface{}{
		"fsType":               cloudProfileConfig.DefaultStorageFsType,
		"volumeBindingMode":    volumeBindingMode,
		"allowVolumeExpansion": true,
	}, nil
}

// getConfigChartValues collects and returns the configuration chart values.
//
// PARAMETERS
// cpConfig    *apis.ControlPlaneConfig         Control plane config struct
// cp          *extensionsv1alpha1.ControlPlane Control plane struct
// cluster     *extensionscontroller.Cluster    Cluster struct
// credentials *warren.Credentials              Credentials instance
func (vp *valuesProvider) getConfigChartValues(
	cpConfig *apis.ControlPlaneConfig,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	credentials *warren.Credentials,
) (map[string]interface{}, error) {
	// Collect config chart values
	values := map[string]interface{}{
		"token":          credentials.CCM().Token,
		"zone":           cpConfig.Zone,
		"apiEndpointURL": cpConfig.APIEndpointURL,
	}

	return values, nil
}

// getControlPlaneChartValues collects and returns the control plane chart values.
//
// PARAMETERS
// cpConfig      *apis.ControlPlaneConfig         Control plane config struct
// infraStatus   *apis.InfrastructureStatus       Infrastructure status struct
// cp            *extensionsv1alpha1.ControlPlane Control plane struct
// cluster       *extensionscontroller.Cluster    Cluster struct
// secretsReader secretsmanager.Reader            Secrets manager reader
// credentials   *warren.Credentials              Credentials instance
// checksums     map[string]string                Checksums
// scaledDown    bool                             True if scaled down
func (vp *valuesProvider) getControlPlaneChartValues(
	cpConfig *apis.ControlPlaneConfig,
	infraStatus *apis.InfrastructureStatus,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	secretsReader secretsmanager.Reader,
	credentials *warren.Credentials,
	checksums map[string]string,
	scaledDown bool,
) (map[string]interface{}, error) {
	ccmValues, err := vp.getCCMChartValues(cpConfig, infraStatus, cp, cluster, secretsReader, checksums, scaledDown)
	if err != nil {
		return nil, err
	}

	csiValues, err := vp.getCSIControllerChartValues(cpConfig, cp, cluster, credentials, checksums, scaledDown)
	if err != nil {
		return nil, err
	}

	values := map[string]interface{}{
		"global": map[string]interface{}{
			"genericTokenKubeconfigSecretName": extensionscontroller.GenericTokenKubeconfigSecretNameFromCluster(cluster),
		},
		warren.CloudControllerManagerName: ccmValues,
		warren.CSIControllerName:          csiValues,
	}

	return values, nil
}

// getCCMChartValues collects and returns the CCM chart values.
//
// PARAMETERS
// cpConfig      *apis.ControlPlaneConfig         Control plane config struct
// infraStatus   *apis.InfrastructureStatus       Infrastructure status struct
// cp            *extensionsv1alpha1.ControlPlane Control plane struct
// cluster       *extensionscontroller.Cluster    Cluster struct
// secretsReader secretsmanager.Reader            Secrets manager reader
// checksums     map[string]string                Checksums
// scaledDown    bool                             True if scaled down
// region        string                           Control plane region
func (vp *valuesProvider) getCCMChartValues(
	cpConfig *apis.ControlPlaneConfig,
	infraStatus *apis.InfrastructureStatus,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	secretsReader secretsmanager.Reader,
	checksums map[string]string,
	scaledDown bool,
) (map[string]interface{}, error) {
	clusterID := vp.calcClusterID(cp)

	ccmSecret, found := secretsReader.Get(warren.CloudControllerManagerServerName)
	if !found {
		return nil, fmt.Errorf("secret %q not found", warren.CloudControllerManagerServerName)
	}

	values := map[string]interface{}{
		"enabled":           true,
		"replicas":          extensionscontroller.GetControlPlaneReplicas(cluster, scaledDown, 1),
		"clusterName":       clusterID,
		"kubernetesVersion": cluster.Shoot.Spec.Kubernetes.Version,
		"serverSecretName":  ccmSecret.Name,
		"zone":              cpConfig.Zone,
		"apiEndpointURL":    cpConfig.APIEndpointURL,
		"podAnnotations": map[string]interface{}{
			"checksum/secret-" + v1beta1constants.SecretNameCloudProvider: checksums[v1beta1constants.SecretNameCloudProvider],
			"checksum/secret-" + warren.CloudProviderConfigName:           checksums[warren.CloudProviderConfigName],
		},
		"podLabels": map[string]interface{}{
			v1beta1constants.LabelPodMaintenanceRestart: "true",
		},
	}

	if cpConfig.CloudControllerManager != nil {
		values["featureGates"] = cpConfig.CloudControllerManager.FeatureGates

		if cpConfig.CloudControllerManager.LogValue != 0 {
			values["logValue"] = cpConfig.CloudControllerManager.LogValue
		}
	}

	return values, nil
}

// getCSIControllerChartValues collects and returns the CSIController chart values.
//
// PARAMETERS
// cp          *extensionsv1alpha1.ControlPlane Control plane struct
// cluster     *extensionscontroller.Cluster    Cluster struct
// credentials *warren.Credentials              Credentials instance
// checksums   map[string]string                Checksums
// scaledDown  bool                             True if scaled down
// region      string                           Control plane region
func (vp *valuesProvider) getCSIControllerChartValues(
	cpConfig *apis.ControlPlaneConfig,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	credentials *warren.Credentials,
	checksums map[string]string,
	scaledDown bool,
) (map[string]interface{}, error) {
	csiClusterID := vp.calcCsiClusterID(cp)

	values := map[string]interface{}{
		"enabled":           true,
		"replicas":          extensionscontroller.GetControlPlaneReplicas(cluster, scaledDown, 1),
		"kubernetesVersion": cluster.Shoot.Spec.Kubernetes.Version,
		"clusterID":         csiClusterID,
		"token":             credentials.CSI().Token,
		"zone":              cpConfig.Zone,
		"apiEndpointURL":    cpConfig.APIEndpointURL,
		// "resizerEnabled":    csiResizerEnabled,
		"podAnnotations": map[string]interface{}{
			"checksum/secret-" + v1beta1constants.SecretNameCloudProvider: checksums[v1beta1constants.SecretNameCloudProvider],
		},
	}

	if cpConfig.CSIDriverController != nil {
		if cpConfig.CSIDriverController.LogValue != 0 {
			values["logValue"] = cpConfig.CSIDriverController.LogValue
		}
	}

	return values, nil
}

// getControlPlaneShootChartValues collects and returns the control plane shoot chart values.
//
// PARAMETERS
// cpConfig    *apis.ControlPlaneConfig         Control plane config struct
// cp          *extensionsv1alpha1.ControlPlane Control plane struct
// cluster     *extensionscontroller.Cluster    Cluster struct
// credentials *warren.Credentials              Credentials instance
func (vp *valuesProvider) getControlPlaneShootChartValues(
	cpConfig *apis.ControlPlaneConfig,
	cp *extensionsv1alpha1.ControlPlane,
	cluster *extensionscontroller.Cluster,
	credentials *warren.Credentials,
) (map[string]interface{}, error) {
	ccmValues := map[string]interface{}{
		"enabled": true,
	}

	csiValues := map[string]interface{}{
		"enabled":           true,
		"clusterID":         vp.calcCsiClusterID(cp),
		"zone":              cpConfig.Zone,
		"apiEndpointURL":    cpConfig.APIEndpointURL,
		"token":             credentials.CSI().Token,
		"kubernetesVersion": cluster.Shoot.Spec.Kubernetes.Version,
	}

	if cpConfig.CSIDriverNode != nil {
		if cpConfig.CSIDriverNode.LogValue != 0 {
			csiValues["logValue"] = cpConfig.CSIDriverNode.LogValue
		}
	}

	values := map[string]interface{}{
		warren.CloudControllerManagerName: ccmValues,
		warren.CSINodeName:                csiValues,
	}

	return values, nil
}

// calcClusterID returns the cluster ID.
//
// PARAMETERS
// cp *extensionsv1alpha1.ControlPlane Control plane struct
func (vp *valuesProvider) calcClusterID(cp *extensionsv1alpha1.ControlPlane) string {
	return cp.Namespace + "-" + vp.gardenID
}

// calcCsiClusterID returns the CSI cluster ID.
//
// PARAMETERS
// cp *extensionsv1alpha1.ControlPlane Control plane struct
func (vp *valuesProvider) calcCsiClusterID(cp *extensionsv1alpha1.ControlPlane) string {
	return shortenID(vp.calcClusterID(cp), 63)
}

// shortenID returns a shortened ID with the given size.
//
// PARAMETERS
// id     string ID
// maxlen int    Maximum length
func shortenID(id string, maxlen int) string {
	if maxlen < 16 {
		panic("maxlen < 16 for shortenID")
	}
	if len(id) <= maxlen {
		return id
	}

	hash := fnv.New64()
	_, _ = hash.Write([]byte(id))
	hashstr := strconv.FormatUint(hash.Sum64(), 36)
	return fmt.Sprintf("%s-%s", id[:62-len(hashstr)], hashstr)
}

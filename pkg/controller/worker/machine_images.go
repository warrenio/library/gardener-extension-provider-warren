/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package worker contains functions used at the worker controller
package worker

import (
	"context"
	"fmt"

	"github.com/gardener/gardener/extensions/pkg/controller/worker"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/transcoder"
)

// findMachineImageName returns the image name for the given name and version values.
//
// PARAMETERS
// ctx     context.Context Execution context
// name    string          Machine image name
// version string          Machine image version
func (w *workerDelegate) findMachineImageName(ctx context.Context, name, version string) (string, error) {
	machineImage, err := transcoder.DecodeMachineImageNameFromCloudProfile(w.cloudProfileConfig, name, version)
	if err == nil {
		return machineImage, nil
	}

	images, err := w.apiClient.VirtualMachine.ListBaseImages()
	if nil != err {
		return "", apis.GetImageErrorFromHttpCallError(err)
	}

	for _, image := range *images {
		if image.OsName != name {
			continue
		}

		for _, imageVersion := range image.Versions {
			if imageVersion.OsVersion == version {
				return fmt.Sprintf("%s-%s", image.OsName, imageVersion.OsVersion), nil
			}
		}
	}

	return "", worker.ErrorMachineImageNotFound(name, version)
}

// UpdateMachineImagesStatus adds machineImages to the `WorkerStatus` resource.
//
// PARAMETERS
// ctx context.Context Execution context
func (w *workerDelegate) UpdateMachineImagesStatus(ctx context.Context) error {
	if w.machineImages == nil {
		if err := w.generateMachineConfig(ctx); err != nil {
			return err
		}
	}

	// Decode the current worker provider status.
	workerStatus, err := transcoder.DecodeWorkerStatusFromWorker(w.worker)
	if err != nil {
		return fmt.Errorf("unable to decode the worker provider status: %w", err)
	}

	workerStatus.MachineImages = w.machineImages

	if err := w.updateProviderStatus(ctx, workerStatus); err != nil {
		return fmt.Errorf("unable to update worker provider status: %w", err)
	}

	return nil
}



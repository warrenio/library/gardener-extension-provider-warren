/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package worker contains functions used at the worker controller
package worker

import (
	"context"

	v1beta1constants "github.com/gardener/gardener/pkg/apis/core/v1beta1/constants"
	kutil "github.com/gardener/gardener/pkg/utils/kubernetes"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren"
	corev1 "k8s.io/api/core/v1"
)

// GetMachineControllerManagerChartValues returns chart values relevant for the MCM instance.
//
// PARAMETERS
// ctx context.Context Execution context
func (w *workerDelegate) GetMachineControllerManagerChartValues(ctx context.Context) (map[string]interface{}, error) {
	namespace := &corev1.Namespace{}

	err := w.client.Get(ctx, kutil.Key(w.worker.Namespace), namespace)
	if err != nil {
		return nil, err
	}

	return map[string]interface{}{
		"providerName": warren.Name,
		"namespace": map[string]interface{}{
			"uid": namespace.UID,
		},
		"podLabels": map[string]interface{}{
			v1beta1constants.LabelPodMaintenanceRestart: "true",
		},
	}, nil
}

// GetMachineControllerManagerShootChartValues returns chart values relevant for the MCM shoot instance.
//
// PARAMETERS
// ctx context.Context Execution context
func (w *workerDelegate) GetMachineControllerManagerShootChartValues(ctx context.Context) (map[string]interface{}, error) {
	return map[string]interface{}{"providerName": warren.Name}, nil
}

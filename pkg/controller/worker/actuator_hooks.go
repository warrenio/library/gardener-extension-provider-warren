/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package worker contains functions used at the worker controller
package worker

import (
	"context"

	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/worker/ensurer"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/controller"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/transcoder"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PreReconcileHook is a hook called at the beginning of the worker reconciliation flow.
//
// PARAMETERS
// _ context.Context Execution context
func (w *workerDelegate) PreReconcileHook(ctx context.Context) error {
	extendedCtx := context.WithValue(ctx, controller.CtxWrapDataKey("MethodData"), &controller.WorkerPreReconcileHookMethodData{})

	err := w.preReconcileHook(extendedCtx)

	if nil != err {
		w.preReconcileHookOnErrorCleanup(extendedCtx, err)
	}

	return err
}

// preReconcileHook is a hook called at the beginning of the worker reconciliation flow.
//
// PARAMETERS
// _ context.Context Execution context
func (w *workerDelegate) preReconcileHook(ctx context.Context) error {
	zonesMap := map[string]bool{}

	for _, pool := range w.worker.Spec.Pools {
		for _, zone := range pool.Zones {
			zonesMap[zone] = true
		}
	}

	var zones []string

	for zone, _ := range zonesMap {
		zones = append(zones, zone)
	}

	workerNetworkUUIDs, err := ensurer.EnsureNetworks(ctx, w.apiClient, w.worker.Namespace, zones)
	if err != nil {
		return err
	}

	workerStatus := &apis.WorkerStatus{
		TypeMeta: metav1.TypeMeta{
			APIVersion: apis.SchemeGroupVersion.String(),
			Kind:       "WorkerStatus",
		},
	}

	if len(workerNetworkUUIDs) > 0 {
		workerStatus.NetworkUUIDs = map[string]*apis.WorkerStatusNetworkUUIDs{}

		for zone, networkUUIDs := range workerNetworkUUIDs {
			workerStatus.NetworkUUIDs[zone] = &apis.WorkerStatusNetworkUUIDs{
				Workers: networkUUIDs.Workers,
			}
		}
	}

	return w.updateProviderStatus(ctx, workerStatus)
}

// preReconcileHookOnErrorCleanup cleans up a failed reconcile request
//
// PARAMETERS
// ctx     context.Context                    Execution context
// infra   *extensionsv1alpha1.Infrastructure Infrastructure struct
// cluster *extensionscontroller.Cluster      Cluster struct
// err     error                              Error encountered
func (w *workerDelegate) preReconcileHookOnErrorCleanup(ctx context.Context, err error) {
	resultData := ctx.Value(controller.CtxWrapDataKey("MethodData")).(*controller.WorkerPreReconcileHookMethodData)

	if nil != resultData.NetworkUUIDs && len(resultData.NetworkUUIDs) > 0 {
		ensurer.EnsureNetworksDeleted(ctx, w.apiClient, w.worker.Namespace, resultData.NetworkUUIDs)
	}
}

// PostReconcileHook is a hook called at the end of the worker reconciliation flow.
//
// PARAMETERS
// _ context.Context Execution context
func (w *workerDelegate) PostReconcileHook(_ context.Context) error {
	return nil
}

// PreDeleteHook is a hook called at the beginning of the worker deletion flow.
//
// PARAMETERS
// _ context.Context Execution context
func (w *workerDelegate) PreDeleteHook(_ context.Context) error {
	return nil
}

// PostDeleteHook is a hook called at the end of the worker deletion flow.
//
// PARAMETERS
// _ context.Context Execution context
func (w *workerDelegate) PostDeleteHook(ctx context.Context) error {
	workerStatus, _ := transcoder.DecodeWorkerStatusFromWorker(w.worker)

	if nil != workerStatus {
		err := ensurer.EnsureNetworksDeleted(ctx, w.apiClient, w.worker.Namespace, workerStatus.NetworkUUIDs)
		if err != nil {
			return err
		}
	}

	return w.updateProviderStatus(ctx, nil)
}

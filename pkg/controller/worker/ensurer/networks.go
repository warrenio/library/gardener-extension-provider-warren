/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package ensurer provides functions used to ensure infrastructure changes to be applied
package ensurer

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/warrenio/library/go-client/warren"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/controller"
)

// EnsureNetworks verifies the network resources requested are available.
//
// PARAMETERS
// ctx       context.Context                    Execution context
// client    *warren.Client                     Warren client
// namespace string                             Shoot namespace
// networks  *apis.InfrastructureConfigNetworks Networks struct
func EnsureNetworks(
	ctx context.Context,
	client *warren.Client,
	namespace string,
	zones []string,
) (map[string]*apis.WorkerStatusNetworkUUIDs, error) {
	networkUUIDs := map[string]*apis.WorkerStatusNetworkUUIDs{ }

	for _, zone := range zones {
		name := fmt.Sprintf("%s-%s-workers", namespace, zone)
		zoneClient := apis.GetReconfiguredClientForLocation(client, zone)

		network, err := apis.GetNetworkByName(ctx, zoneClient, name)
		if errors.Is(err, apis.ErrNetworkNotFound) {
			network, err = zoneClient.Network.CreateNetwork(name)
			if nil != err {
				return networkUUIDs, apis.GetNetworkErrorFromHttpCallError(err)
			}

			resultData := ctx.Value(controller.CtxWrapDataKey("MethodData")).(*controller.WorkerPreReconcileHookMethodData)

			if nil == resultData.NetworkUUIDs {
				resultData.NetworkUUIDs = map[string]*apis.WorkerStatusNetworkUUIDs{ }
			}

			resultData.NetworkUUIDs[zone] = &apis.WorkerStatusNetworkUUIDs{ Workers: network.Uuid }
		} else if nil != err {
			return networkUUIDs, err
		}

		networkUUIDs[zone] = &apis.WorkerStatusNetworkUUIDs{ Workers: network.Uuid }
	}

	return networkUUIDs, nil
}

// EnsureNetworksDeleted removes any previously created network resources.
//
// PARAMETERS
// ctx       context.Context                                   Execution context
// client    *warren.Client                                    Warren client
// namespace string                                            Shoot namespace
// networks  map[string]*apis.WorkerStatusNetworkUUIDs Network IDs struct per zone
func EnsureNetworksDeleted(
	ctx context.Context,
	client *warren.Client,
	namespace string,
	networks map[string]*apis.WorkerStatusNetworkUUIDs,
) error {
	for zone, networkUUIDs := range networks {
		zoneClient := apis.GetReconfiguredClientForLocation(client, zone)

		if "" != networkUUIDs.Workers {
			err := apis.GetNetworkErrorFromHttpCallError(zoneClient.Network.DeleteNetworkByUUID(networkUUIDs.Workers))
			if errors.Is(err, apis.ErrNetworkNotFound) {
				return nil
			} else if nil != err {
				return err
			}
		}
	}

	return nil
}

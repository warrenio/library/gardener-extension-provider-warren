/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package controller provides Kubernetes controller configuration structures used for command execution
package controller

import (
	"github.com/gardener/gardener/extensions/pkg/controller/cmd"
	"github.com/gardener/gardener/extensions/pkg/controller/controlplane"
	"github.com/gardener/gardener/extensions/pkg/controller/healthcheck"
	"github.com/gardener/gardener/extensions/pkg/controller/heartbeat"
	"github.com/gardener/gardener/extensions/pkg/controller/infrastructure"
	"github.com/gardener/gardener/extensions/pkg/controller/worker"
	warrencontrolplane "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/controlplane"
	warrenhealthcheck "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/healthcheck"
	warreninfrastructure "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/infrastructure"
	warrenworker "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/worker"
)

// ControllerSwitchOptions are the cmd.SwitchOptions for the provider controllers.
func ControllerSwitchOptions() *cmd.SwitchOptions {
	return cmd.NewSwitchOptions(
		cmd.Switch(controlplane.ControllerName, warrencontrolplane.AddToManager),
		cmd.Switch(healthcheck.ControllerName, warrenhealthcheck.AddToManager),
		cmd.Switch(heartbeat.ControllerName, heartbeat.AddToManager),
		cmd.Switch(infrastructure.ControllerName, warreninfrastructure.AddToManager),
		cmd.Switch(worker.ControllerName, warrenworker.AddToManager),
	)
}

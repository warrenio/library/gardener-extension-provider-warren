/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package main provides the application's entry point
package main

import (
	"fmt"
	"os"

	"github.com/gardener/gardener/extensions/pkg/controller/cmd"
	"github.com/gardener/gardener/extensions/pkg/util"
	webhookcmd "github.com/gardener/gardener/extensions/pkg/webhook/cmd"
	"github.com/gardener/gardener/pkg/apis/core/install"
	v1beta1constants "github.com/gardener/gardener/pkg/apis/core/v1beta1/constants"
	gardenerhealthz "github.com/gardener/gardener/pkg/healthz"
	"github.com/spf13/cobra"
	warrencmd "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/cmd/admission"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren"
	warrenapisinstall "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/install"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/component-base/config"
	"k8s.io/component-base/version/verflag"
	"sigs.k8s.io/controller-runtime/pkg/cache"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/cluster"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	runtimelog "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/manager/signals"
)

// admissionName is the name of the admission component.
const admissionName = "admission-warren"

var log = runtimelog.Log.WithName(fmt.Sprintf("gardener-extension-%s", admissionName))

func main() {
	ctx := signals.SetupSignalHandler()

	restOpts := &cmd.RESTOptions{}

	mgrOpts := &cmd.ManagerOptions{
		LeaderElection:          true,
		LeaderElectionID:        cmd.LeaderElectionNameID(admissionName),
		LeaderElectionNamespace: os.Getenv("LEADER_ELECTION_NAMESPACE"),
		WebhookServerPort:       443,
		MetricsBindAddress:      ":8080",
		HealthBindAddress:       ":8081",
		WebhookCertDir:          "/tmp/admission-gcp-cert",
	}

	webhookServerOptions := &webhookcmd.ServerOptions{Namespace: os.Getenv("WEBHOOK_CONFIG_NAMESPACE")}
	webhookSwitches := warrencmd.SwitchOptions()

	webhookOptions := webhookcmd.NewAddToManagerOptions(
		admissionName,
		"",
		nil,
		webhookServerOptions,
		webhookSwitches,
	)

	aggOption := cmd.NewOptionAggregator(
		restOpts,
		mgrOpts,
		webhookOptions,
	)

	cmdDefinition := &cobra.Command{
		Use: fmt.Sprintf("admission-%s", warren.Type),

		PreRun: func(cmdDefinition *cobra.Command, args []string) {
			verflag.PrintAndExitIfRequested()
		},

		RunE: func(cmdDefinition *cobra.Command, args []string) error {
			if err := aggOption.Complete(); err != nil {
				return fmt.Errorf("Error completing options: %w", err)
			}

			util.ApplyClientConnectionConfigurationToRESTConfig(&config.ClientConnectionConfiguration{
				QPS:   100.0,
				Burst: 130,
			}, restOpts.Completed().Config)

			managerOptions := mgrOpts.Completed().Options()

			// Operators can enable the source cluster option via SOURCE_CLUSTER environment variable.
			// In-cluster config will be used if no SOURCE_KUBECONFIG is specified.
			//
			// The source cluster is for instance used by Gardener's certificate controller, to maintain certificate
			// secrets in a different cluster ('runtime-garden') than the cluster where the webhook configurations
			// are maintained ('virtual-garden').
			var sourceClusterConfig *rest.Config
			if sourceClusterEnabled := os.Getenv("SOURCE_CLUSTER"); sourceClusterEnabled != "" {
				log.Info("Configuring source cluster option")

				var err error
				sourceClusterConfig, err = clientcmd.BuildConfigFromFlags("", os.Getenv("SOURCE_KUBECONFIG"))
				if err != nil {
					return err
				}
				managerOptions.LeaderElectionConfig = sourceClusterConfig
			} else {
				// Restrict the cache for secrets to the configured namespace to avoid the need for cluster-wide list/watch permissions.
				managerOptions.Cache = cache.Options{
					ByObject: map[client.Object]cache.ByObject{
						&corev1.Secret{}: {Namespaces: map[string]cache.Config{webhookOptions.Server.Completed().Namespace: {}}},
					},
				}
			}

			mgr, err := manager.New(restOpts.Completed().Config, managerOptions)
			if err != nil {
				return fmt.Errorf("Could not instantiate manager: %w", err)
			}

			install.Install(mgr.GetScheme())

			if err := warrenapisinstall.AddToScheme(mgr.GetScheme()); err != nil {
				return fmt.Errorf("Could not update manager scheme: %w", err)
			}

			log.Info("Setting up webhook server")

			var sourceCluster cluster.Cluster
			if sourceClusterConfig != nil {
				sourceCluster, err = cluster.New(sourceClusterConfig, func(opts *cluster.Options) {
					opts.Logger = log
					opts.Cache.DefaultNamespaces = map[string]cache.Config{v1beta1constants.GardenNamespace: {}}
				})
				if err != nil {
					return err
				}

				if err := mgr.AddReadyzCheck("source-informer-sync", gardenerhealthz.NewCacheSyncHealthz(sourceCluster.GetCache())); err != nil {
					return err
				}

				if err = mgr.Add(sourceCluster); err != nil {
					return err
				}
			}

			log.Info("Setting up webhook server")

			if _, err := webhookOptions.Completed().AddToManager(ctx, mgr, sourceCluster); err != nil {
				return err
			}

			if err := mgr.AddHealthzCheck("ping", healthz.Ping); err != nil {
				return err
			}

			if err := mgr.AddReadyzCheck("informer-sync", gardenerhealthz.NewCacheSyncHealthz(mgr.GetCache())); err != nil {
				return fmt.Errorf("could not add readycheck for informers: %w", err)
			}

			if err := mgr.AddReadyzCheck("webhook-server", mgr.GetWebhookServer().StartedChecker()); err != nil {
				return fmt.Errorf("could not add readycheck of webhook to manager: %w", err)
			}

			return mgr.Start(ctx)
		},
	}

	cmdFlags := cmdDefinition.Flags()
	aggOption.AddFlags(cmdFlags)
	verflag.AddFlags(cmdFlags)

	if err := cmdDefinition.Execute(); err != nil {
		log.Error(err, "Error executing command")
		os.Exit(1)
	}
}

/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package main provides the application's entry point
package main

import (
	"fmt"
	"os"

	druidv1alpha1 "github.com/gardener/etcd-druid/api/v1alpha1"
	"github.com/gardener/gardener/extensions/pkg/controller"
	"github.com/gardener/gardener/extensions/pkg/controller/cmd"
	"github.com/gardener/gardener/extensions/pkg/controller/controlplane/genericactuator"
	"github.com/gardener/gardener/extensions/pkg/controller/heartbeat"
	heartbeatcmd "github.com/gardener/gardener/extensions/pkg/controller/heartbeat/cmd"
	"github.com/gardener/gardener/extensions/pkg/util"
	webhookcmd "github.com/gardener/gardener/extensions/pkg/webhook/cmd"
	"github.com/gardener/gardener/pkg/client/kubernetes"
	gardenerhealthz "github.com/gardener/gardener/pkg/healthz"
	"github.com/gardener/gardener/pkg/logger"
	machinev1alpha1 "github.com/gardener/machine-controller-manager/pkg/apis/machine/v1alpha1"
	"github.com/spf13/cobra"
	warrencmd "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/cmd/controller"
	warrencontrolplane "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/controlplane"
	warrenhealthcheck "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/healthcheck"
	warreninfrastructure "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/infrastructure"
	warrenworker "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/controller/worker"
	"gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren"
	warrenapisinstall "gitlab.com/warrenio/library/gardener-extension-provider-warren/pkg/warren/apis/install"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	autoscalingv1beta2 "k8s.io/autoscaler/vertical-pod-autoscaler/pkg/apis/autoscaling.k8s.io/v1beta2"
	"k8s.io/component-base/version/verflag"
	"sigs.k8s.io/controller-runtime/pkg/cluster"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/manager/signals"
)

// main is the executable entry point.
func main() {
	log.SetLogger(logger.MustNewZapLogger(logger.InfoLevel, logger.FormatJSON))

	ctx := signals.SetupSignalHandler()

	generalOpts := &cmd.GeneralOptions{}
	restOpts := &cmd.RESTOptions{}

	mgrOpts := &cmd.ManagerOptions{
		LeaderElection:          true,
		LeaderElectionID:        cmd.LeaderElectionNameID(warren.Name),
		LeaderElectionNamespace: os.Getenv("LEADER_ELECTION_NAMESPACE"),
		WebhookServerPort:       443,
		WebhookCertDir:          "/tmp/gardener-extensions-cert",
		HealthBindAddress:       ":8081",
		MetricsBindAddress:      ":8080",
	}

	configFileOpts := &warrencmd.ConfigOptions{}

	// options for the infrastructure controller
	infraCtrlOpts := &cmd.ControllerOptions{
		MaxConcurrentReconciles: 5,
	}
	reconcileOpts := &cmd.ReconcilerOptions{}

	// options for the health care controller
	healthCareCtrlOpts := &cmd.ControllerOptions{
		MaxConcurrentReconciles: 5,
	}

	// options for the control plane controller
	controlPlaneCtrlOpts := &cmd.ControllerOptions{
		MaxConcurrentReconciles: 5,
	}

	// options for the heartbeat controller
	heartbeatCtrlOpts := &heartbeatcmd.Options{
		ExtensionName:        warren.Name,
		RenewIntervalSeconds: 30,
		Namespace:            os.Getenv("LEADER_ELECTION_NAMESPACE"),
	}

	// options for the worker controller
	workerCtrlOpts := &cmd.ControllerOptions{
		MaxConcurrentReconciles: 5,
	}

	// options for the webhook server
	webhookServerOptions := &webhookcmd.ServerOptions{
		Namespace: os.Getenv("WEBHOOK_CONFIG_NAMESPACE"),
	}

	controllerSwitches := warrencmd.ControllerSwitchOptions()
	webhookSwitches := warrencmd.WebhookSwitchOptions()

	webhookOptions := webhookcmd.NewAddToManagerOptions(
		warren.Name,
		genericactuator.ShootWebhooksResourceName,
		genericactuator.ShootWebhookNamespaceSelector(warren.Type),
		webhookServerOptions,
		webhookSwitches,
	)

	aggOption := cmd.NewOptionAggregator(
		generalOpts,
		restOpts,
		mgrOpts,
		cmd.PrefixOption("controlplane-", controlPlaneCtrlOpts),
		cmd.PrefixOption("infrastructure-", infraCtrlOpts),
		cmd.PrefixOption("worker-", workerCtrlOpts),
		cmd.PrefixOption("healthcheck-", healthCareCtrlOpts),
		cmd.PrefixOption("heartbeat-", heartbeatCtrlOpts),
		controllerSwitches,
		configFileOpts,
		reconcileOpts,
		webhookOptions,
	)

	cmdDefinition := &cobra.Command{
		Use: fmt.Sprintf("%s-controller-manager", warren.Name),

		PreRun: func(cmdDefinition *cobra.Command, args []string) {
			verflag.PrintAndExitIfRequested()
		},

		RunE: func(cmdDefinition *cobra.Command, args []string) error {
			if err := aggOption.Complete(); err != nil {
				return fmt.Errorf("Error completing options: %w", err)
			}

			if err := heartbeatCtrlOpts.Validate(); err != nil {
				return err
			}

			util.ApplyClientConnectionConfigurationToRESTConfig(configFileOpts.Completed().Config.ClientConnection, restOpts.Completed().Config)

			mgrOptions := mgrOpts.Completed().Options()

			configFileOpts.Completed().ApplyMetricsBindAddress(&mgrOptions.Metrics.BindAddress)

			mgr, err := manager.New(restOpts.Completed().Config, mgrOptions)
			if err != nil {
				return fmt.Errorf("Could not instantiate manager: %w", err)
			}

			scheme := mgr.GetScheme()
			if err := controller.AddToScheme(scheme); err != nil {
				return fmt.Errorf("Could not update manager scheme: %w", err)
			}
			if err := warrenapisinstall.AddToScheme(scheme); err != nil {
				return fmt.Errorf("Could not update manager scheme: %w", err)
			}
			if err := druidv1alpha1.AddToScheme(scheme); err != nil {
				return fmt.Errorf("Could not update manager scheme: %w", err)
			}
			if err := machinev1alpha1.AddToScheme(scheme); err != nil {
				return fmt.Errorf("Could not update manager scheme: %w", err)
			}
			if err := autoscalingv1beta2.AddToScheme(scheme); err != nil {
				return fmt.Errorf("Could not update manager scheme: %w", err)
			}

			// add common meta types to schema for controller-runtime to use v1.ListOptions
			metav1.AddToGroupVersion(scheme, machinev1alpha1.SchemeGroupVersion)

			log := mgr.GetLogger()
			log.Info("Getting REST config for garden")
			gardenRESTConfig, err := kubernetes.RESTConfigFromKubeconfigFile(os.Getenv("GARDEN_KUBECONFIG"), kubernetes.AuthTokenFile)
			if err != nil {
				return err
			}

			log.Info("Setting up cluster object for garden")
			gardenCluster, err := cluster.New(gardenRESTConfig, func(opts *cluster.Options) {
				opts.Scheme = kubernetes.GardenScheme
				opts.Logger = log
			})
			if err != nil {
				return fmt.Errorf("Failed creating garden cluster object: %w", err)
			}

			log.Info("Adding garden cluster to manager")
			if err := mgr.Add(gardenCluster); err != nil {
				return fmt.Errorf("Failed adding garden cluster to manager: %w", err)
			}

			log.Info("Adding controllers to manager")
			configFileOpts.Completed().ApplyGardenId(&warrencontrolplane.DefaultAddOptions.GardenId)
			configFileOpts.Completed().ApplyGardenId(&warreninfrastructure.DefaultAddOptions.GardenId)
			configFileOpts.Completed().ApplyHealthCheckConfig(&warrenhealthcheck.DefaultAddOptions.HealthCheckConfig)
			controlPlaneCtrlOpts.Completed().Apply(&warrencontrolplane.DefaultAddOptions.Controller)
			healthCareCtrlOpts.Completed().Apply(&warrenhealthcheck.DefaultAddOptions.Controller)
			heartbeatCtrlOpts.Completed().Apply(&heartbeat.DefaultAddOptions)
			infraCtrlOpts.Completed().Apply(&warreninfrastructure.DefaultAddOptions.Controller)
			reconcileOpts.Completed().Apply(&warreninfrastructure.DefaultAddOptions.IgnoreOperationAnnotation)
			reconcileOpts.Completed().Apply(&warrencontrolplane.DefaultAddOptions.IgnoreOperationAnnotation)
			reconcileOpts.Completed().Apply(&warrenworker.DefaultAddOptions.IgnoreOperationAnnotation)
			workerCtrlOpts.Completed().Apply(&warrenworker.DefaultAddOptions.Controller)
			warrenworker.DefaultAddOptions.GardenCluster = gardenCluster

			_, err = webhookOptions.Completed().AddToManager(ctx, mgr, nil)
			if err != nil {
				return fmt.Errorf("Could not add webhooks to manager: %w", err)
			}
			// no webhook/shoot required
			// warrencontrolplane.DefaultAddOptions.ShootWebhookConfig = shootWebhookConfig
			warrencontrolplane.DefaultAddOptions.WebhookServerNamespace = webhookOptions.Server.Namespace

			if err := controllerSwitches.Completed().AddToManager(ctx, mgr); err != nil {
				return fmt.Errorf("Could not add controllers to manager: %w", err)
			}

			if err := mgr.AddHealthzCheck("ping", healthz.Ping); err != nil {
				return err
			}

			if err := mgr.AddReadyzCheck("informer-sync", gardenerhealthz.NewCacheSyncHealthz(mgr.GetCache())); err != nil {
				return fmt.Errorf("could not add readycheck for informers: %w", err)
			}

			if err := mgr.AddReadyzCheck("webhook-server", mgr.GetWebhookServer().StartedChecker()); err != nil {
				return fmt.Errorf("could not add readycheck of webhook to manager: %w", err)
			}

			if err := mgr.Start(ctx); err != nil {
				return fmt.Errorf("Error running manager: %w", err)
			}

			return nil
		},
	}

	cmdFlags := cmdDefinition.Flags()
	aggOption.AddFlags(cmdFlags)
	verflag.AddFlags(cmdFlags)

	if err := cmdDefinition.Execute(); err != nil {
		log.Log.Error(err, "Error executing command")
		os.Exit(1)
	}
}

# Gardener Extension Provider Warren

DISCLAIMER: Manual deployment of the manager to any Warren supporting platform is currently not officially supported.

Project Gardener implements the automated management and operation of [Kubernetes](https://kubernetes.io) clusters as a service. Its main principle is to leverage Kubernetes concepts for all of its tasks.

The Gardener Extension Provider Warren is an out of tree [implementation](https://gardener.cloud) for the Warren platform.

## Driver Deployment

Please see `example` to how to deploy the Gardener Extension Provider Warren.

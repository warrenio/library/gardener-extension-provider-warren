# builder
FROM golang:alpine AS builder

ENV BINARY_PATH=/go/bin
WORKDIR /go/src/gitlab.com/warrenio/library/gardener-extension-provider-warren

RUN apk add bash git make

COPY . .
RUN make build

# base
FROM gcr.io/distroless/static-debian11:nonroot as base

WORKDIR /

# gardener-extension-provider-warren
FROM base AS gardener-extension-provider-warren
LABEL org.opencontainers.image.source="https://gitlab.com/warrenio/library/gardener-extension-provider-warren"

COPY charts /charts
COPY --from=builder /go/bin/gardener-extension-provider-warren /bin/gardener-extension-provider-warren
COPY --from=builder /go/bin/gardener-extension-admission-warren /bin/gardener-extension-admission-warren
ENTRYPOINT ["/bin/gardener-extension-provider-warren"]

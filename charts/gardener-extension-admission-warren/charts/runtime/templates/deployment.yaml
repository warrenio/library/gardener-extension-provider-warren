apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "name" . }}
  namespace: {{ .Release.Namespace }}
  labels:
{{ include "labels" . | indent 4 }}
spec:
  revisionHistoryLimit: 0
  replicas: {{ .Values.global.replicaCount }}
  selector:
    matchLabels:
{{ include "labels" . | indent 6 }}
  template:
    metadata:
      annotations:
        {{- if .Values.global.kubeconfig }}
        checksum/gardener-extension-admission-warren-kubeconfig: {{ include (print $.Template.BasePath "/secret-kubeconfig.yaml") . | sha256sum }}
        {{- end }}
      labels:
{{ include "labels" . | indent 8 }}
    spec:
      serviceAccountName: {{ include "name" . }}
      {{- if .Values.global.kubeconfig }}
      automountServiceAccountToken: false
      {{- end }}
      containers:
      - name: {{ include "name" . }}
        image: {{ include "image" .Values.global.image }}
        imagePullPolicy: {{ .Values.global.image.pullPolicy }}
        command:
        - /bin/gardener-extension-admission-warren
        - --webhook-config-server-port={{ .Values.global.webhookConfig.serverPort }}
        {{- if .Values.global.virtualGarden.enabled }}
        - --webhook-config-mode=url
        - --webhook-config-url={{ printf "%s.%s" (include "name" .) (.Release.Namespace) }}
        {{- else }}
        - --webhook-config-mode=service
        {{- end }}
        - --webhook-config-namespace={{ .Release.Namespace }}
        {{- if .Values.global.kubeconfig }}
        - --kubeconfig=/etc/gardener-extension-admission-warren/kubeconfig/kubeconfig
        {{- end }}
        {{- if .Values.healthPort }}
        - --health-bind-address=:{{ .Values.healthPort }}
        {{- end }}
        - --leader-election-id={{ include "leaderelectionid" . }}
        {{- if .Values.global.virtualGarden.enabled }}
        env:
        - name: SOURCE_CLUSTER
          value: enabled
        {{- end }}
        ports:
        - name: webhook-server
          containerPort: {{ .Values.global.webhookConfig.serverPort }}
          protocol: TCP
        {{- if .Values.healthPort }}
        readinessProbe:
          httpGet:
            path: /readyz
            port: {{ .Values.healthPort }}
            scheme: HTTP
          initialDelaySeconds: 5
          failureThreshold: 5
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 5
        {{- end }}
{{- if .Values.global.resources }}
        resources:
{{ toYaml .Values.global.resources | nindent 10 }}
{{- end }}
        volumeMounts:
        {{- if .Values.global.kubeconfig }}
        - name: gardener-extension-admission-warren-kubeconfig
          mountPath: /etc/gardener-extension-admission-warren/kubeconfig
          readOnly: true
        {{- end }}
        {{- if .Values.global.serviceAccountTokenVolumeProjection.enabled }}
        - name: service-account-token
          mountPath: /var/run/secrets/projected/serviceaccount
          readOnly: true
        {{- end }}
      volumes:
      - name: gardener-extension-admission-warren-cert
        secret:
          secretName: gardener-extension-admission-warren-cert
          defaultMode: 420
      {{- if .Values.global.kubeconfig }}
      - name: gardener-extension-admission-warren-kubeconfig
        secret:
          secretName: gardener-extension-admission-warren-kubeconfig
          defaultMode: 420
      {{- end }}
      {{- if .Values.global.serviceAccountTokenVolumeProjection.enabled }}
      - name: service-account-token
        projected:
          sources:
          - serviceAccountToken:
              path: token
              expirationSeconds: {{ .Values.global.serviceAccountTokenVolumeProjection.expirationSeconds }}
              {{- if .Values.global.serviceAccountTokenVolumeProjection.audience }}
              audience: {{ .Values.global.serviceAccountTokenVolumeProjection.audience }}
              {{- end }}
      {{- end }}
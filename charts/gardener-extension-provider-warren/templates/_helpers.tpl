{{- define "name" -}}
gardener-extension-provider-warren
{{- end -}}

{{- define "labels.app.key" -}}
app.kubernetes.io/name
{{- end -}}
{{- define "labels.app.value" -}}
{{ include "name" . }}
{{- end -}}

{{- define "labels" -}}
{{ include "labels.app.key" . }}: {{ include "labels.app.value" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{-  define "image" -}}
  {{- if hasPrefix "sha256:" .Values.image.tag }}
  {{- printf "%s@%s" .Values.image.repository .Values.image.tag }}
  {{- else }}
  {{- printf "%s:%s" .Values.image.repository .Values.image.tag }}
  {{- end }}
{{- end }}

{{- define "deploymentversion" -}}
apps/v1
{{- end -}}

{{- define "policyversion" -}}
{{- if semverCompare ">= 1.21-0" .Capabilities.KubeVersion.GitVersion }}
policy/v1
{{- else }}
policy/v1beta1
{{- end }}
{{- end -}}

{{- define "storageclassversion" -}}
{{- if semverCompare ">= 1.13-0" .Capabilities.KubeVersion.GitVersion -}}
storage.k8s.io/v1
{{- else -}}
storage.k8s.io/v1beta1
{{- end -}}
{{- end -}}

{{- define "csi-driver-node.extensionsGroup" -}}
extensions.gardener.cloud
{{- end -}}

{{- define "csi-driver-node.name" -}}
provider-warren
{{- end -}}

{{- define "csi-driver-node.provisioner" -}}
csi.storage.warren.io
{{- end -}}

{{- define "cloud-provider-config" -}}
global:
  token: {{ .Values.token }}
  {{- if .Values.apiEndpointURL }}
  apiEndpointURL: {{ .Values.apiEndpointURL | quote }}
  {{- end }}

{{- if .Values.zone }}
labels:
  zone: {{ .Values.zone | quote }}
{{- end }}
{{- end -}}
